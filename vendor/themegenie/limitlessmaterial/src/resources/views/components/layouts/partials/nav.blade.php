@php
use Carbon\Carbon;
$user = Auth::user();
$registrationTimestamp = $user->created_at;
$notifications = App\Models\Notification::with('user')->where('user_id', '!=', Auth::id())
               ->where('created_at', '>=', $registrationTimestamp)
               ->orderByDesc('created_at')->get();


// for showing notification time
function getTimeAgo($dateTime)
    {
        $time = Carbon::parse($dateTime);
        $now = Carbon::now();

        $diffInMinutes = $time->diffInMinutes($now);
        $diffInHours = $time->diffInHours($now);
        $diffInDays = $time->diffInDays($now);

        if ($diffInMinutes < 60) {
            return $diffInMinutes . ' minutes ago';
        } elseif ($diffInHours < 24) {
            return $diffInHours . ' hours ago';
        } elseif ($diffInDays < 2) {
            return 'Yesterday';
        } elseif ($diffInDays < 7) {
            return $time->format('l'); // Day of the week (e.g., Sunday)
        } else {
            return $time->format('D, d F Y'); // Custom format for older dates
        }
    }

@endphp
<div class="navbar-top mb-3">


<div class="navbar navbar-expand-md navbar-light bg-light fixed-top">
		<div class="navbar-brand">
			<a href="index.html" class="d-inline-block">
            <h6 class="text-light ml-5"> <span style="color:#FF7518;">Pixel</span><span style="color:black;">Harmony</span></h6>
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block legitRipple">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>
			</ul>

			<span class="navbar-text ml-md-3">
				<span class="badge badge-mark border-orange-600 mr-2"></span>
				Hello, {{auth()->user()->name}}..!
			</span>

			<ul class="navbar-nav ml-md-auto">

			<li class="nav-item">
											<a href="#" class="navbar-nav-link legitRipple" data-toggle="dropdown">
												<i class="icon-bell2"></i>
												<span class="d-xl-none ml-2">Notifications</span>
												<span class="badge badge-pill bg-warning-400 ml-auto ml-xl-0">
												{{ Auth::user()->notifications->where('pivot.is_read', false)->where('user_id', '!=', Auth::id())->count() }}
                                                </span>
												</span>
											</a>

											<div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
						<div class="dropdown-content-header">
							<span class="font-size-sm line-height-sm text-uppercase font-weight-semibold">Latest activity</span>
						</div>

						<div class="dropdown-content-body dropdown-scrollable">
							<ul class="media-list">
							@php
                                // Take the last 5 notifications
                                $lastNotifications = $notifications->take(5);
                            @endphp

							@if(count($lastNotifications)>0)

								@foreach($lastNotifications as $notification)
							

                                @php
                                 $isUnread = Auth::user()->notifications->contains(function ($item) use ($notification) {
                                   return $item->id === $notification->id && !$item->pivot->is_read;
                                 });
                                 @endphp

                                @if ($isUnread)
								
								<li class="media" onclick="window.location.href='{{ route('notify.click', ['id' => $notification->id]) }}';">
                                 <div class="mr-3">
								 <span class="badge badge-mark border-orange-600 mr-2"></span>
                                     <a href="{{ route('profile.user2', ['id' => $notification->user->id]) }}">
                                     @if($notification->user->profile_picture === "https://cdn-icons-png.flaticon.com/512/3135/3135715.png")
                                         <img src="https://cdn-icons-png.flaticon.com/512/3135/3135715.png" class="rounded-circle" width="40" height="40" alt="">
                                     @else 
                                         <img src="{{ asset('images/profiles/' . $notification->user->profile_picture) }}" class="rounded-circle" width="40" height="40" alt="">
                                     @endif
                                     </a>
                                 </div>

                                 <div class="media-body">
                                     <b><a href="{{ route('profile.user2', ['id' => $notification->user->id]) }}">
                                         {{ $notification->user->name }}
                                     </a> uploaded a new wallpaper</b>
                                     <div class="font-size-sm text-muted mt-1">{{ getTimeAgo($notification->created_at) }}</div>
                                 </div>
                             </li>
								

								@else
								<li class="media">
									<div class="mr-3">
									<span class="badge badge-mark border-grey-400 mr-2"></span>
									<a href="{{ route('profile.user2', ['id' => $notification->user->id]) }}">
									@if($notification->user->profile_picture === "https://cdn-icons-png.flaticon.com/512/3135/3135715.png")
                                      <img src="https://cdn-icons-png.flaticon.com/512/3135/3135715.png" class="rounded-circle" width="40" height="40" alt="">
                                    @else 
                                    <img src="{{ asset('images/profiles/' . $notification->user->profile_picture) }}" class="rounded-circle" width="40" height="40" alt="">
                                    @endif
									</a>
									</div>

									<div class="media-body">
									
									<a href="{{ route('profile.user2', ['id' => $notification->user->id]) }}">
                                      {{ $notification->user->name }}
                                    </a> uploaded a new wallpaper
									
                                     <div class="font-size-sm text-muted mt-1">{{ getTimeAgo($notification->created_at) }}</div>
									</div>
								</li>
								@endif
								@endforeach
								@else
								<div class="text-center">
								<h6>No Notification</h6>
								</div>
							
								@endif

							</ul>
						</div>
						@if(count($notifications)>0)
						<div class="dropdown-content-footer bg-light">
							<a href="{{route('notification.all')}}" class="font-size-sm line-height-sm text-uppercase font-weight-semibold text-grey mr-auto">All activity</a>
						</div>
						@else
						<div class="dropdown-content-footer bg-light">
							<a href="#" class="font-size-sm line-height-sm text-uppercase font-weight-semibold text-grey mr-auto"></a>
						</div>
						@endif
				</li>



			<li class="nav-item m-2">
											
				
					@if (Route::has('login'))
				
					@auth
						<form method="POST" action="{{ route('logout') }}">
                            @csrf

							<button type="submit" href="{{ route('logout') }}"  style="color: black; background: none; border: none; outline: none;">
								<i class="icon-switch2"></i>
							</button>
                            
                        </form>
						
					@endauth
				
			@endif
				
</li>
	
					
				
			
		</ul>
	</div>
</div>

</div>