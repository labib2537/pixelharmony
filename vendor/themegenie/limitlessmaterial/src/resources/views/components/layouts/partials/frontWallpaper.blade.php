@php
$wallpapers = App\Models\Wallpaper::orderBy('id', 'desc')->paginate(24);
@endphp
<x-sg-wallpaperstyle />

<section class="service full-height" id="wallpaper">
        <div class="container">
            <h2 class="text-center pb-5">Wallpapers</h2>

            <div class="row" id="wallpaperRow">
        @foreach($wallpapers as $wallpaper)
        <div class="col-xl-4 col-md-12 col-lg-6 col-sm-12 mb-3">
        <div class="card card-img-actions rounded-0 border-0">
        <img class="card-img-top img-fluid  rounded-0 border-0" src="{{ asset('uploads/' . $wallpaper->image) }}" alt="${wallpaper.wallpaper_name}">
        <div class="card-img-actions-overlay card-img-top">
            <span class="badge bg-success position-absolute top-0 start-0 p-2  rounded-0 border-0">{{$wallpaper->category}}</span>
            <button type="button" class="btn btn-outline bg-white text-white border-white border-2 legitRipple m-1" data-toggle="modal" data-target="#{{$wallpaper->uuid}}"><i class="icon-eye"></i></button>
            <a href="{{ asset('uploads/' . $wallpaper->image) }}" download class="btn btn-outline bg-white text-white border-white border-2 legitRipple m-1"><i class="icon-download4"></i></a>

            <div class="media position-absolute bottom-0 start-0 p-2 d-flex flex-wrap nowrap">
                <div class="mr-2">
                    <a href="{{route('login')}}">
                        @if($wallpaper->user->profile_picture === "https://cdn-icons-png.flaticon.com/512/3135/3135715.png")
                            <img src="https://cdn-icons-png.flaticon.com/512/3135/3135715.png" class="rounded-circle" width="40" height="40" alt="">
                        @else 
                            <img src="{{ asset('images/profiles/' . $wallpaper->user->profile_picture) }}" class="rounded-circle" width="40" height="40" alt="">
                        @endif
                    </a>
                </div>
                <div class="body">
                    <div class="m-2">{{ $wallpaper->user->name }}</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="{{$wallpaper->uuid}}" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">{{ $wallpaper->wallpaper_name }}</h6>
                <button type="button" class="close border-0 bg-white" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <img class="card-img-top img-fluid" src="{{ asset('uploads/' . $wallpaper->image) }}" >
            </div>
        </div>
    </div>
</div>

        @endforeach 
    </div>

    <div class="pagination-container d-flex justify-content-center">
    {{ $wallpapers->onEachSide(1)->links('pagination::bootstrap-4') }}
</div>


            
        </div>
    </section>
    

    <x-sg-js />




    <!-- Blog & News end -->
