  <!-- Footer Section -->

    <!-- Footer -->

    <footer class="text-center text-lg-start bg-dark text-light" id="ft">
        <!-- Section: Social media -->
        <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
            <!-- Left -->
            <div class="me-5 d-none d-lg-block">
                <span>Get connected with us on social networks:</span>
            </div>
            <!-- Left -->

            <!-- Right -->
            <div class="foot-icon">
                <a href="" class="me-4 text-reset">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="" class="me-4 text-reset">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="" class="me-4 text-reset">
                    <i class="fab fa-google"></i>
                </a>

                <a href="" class="me-4 text-reset">
                    <i class="fab fa-linkedin"></i>
                </a>
            </div>
            <!-- Right -->
        </section>
        <!-- Section: Social media -->

        <!-- Section: Links  -->
        <section class="">
            <div class="container text-center text-md-start mt-5">
                <!-- Grid row -->
                <div class="row mt-3">
                    <!-- Grid column -->
                    <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4">
                        <!-- Content -->
                        <h4 class=" mb-4">
                            <span style="color: #FFA500;">Pixel</span>Harmony
                        </h4>
                        <p class="text-light">
                        Explore an extensive collection of stunning wallpapers, curated to cater to every taste. 
                        Create an account, save your favorites, and enjoy daily featured wallpapers on our user-friendly web app.
                        </p>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4">
                        <!-- Links -->
                        <h6 class="text-uppercase fw-bold mb-4">
                            Services
                        </h6>
                        <p >
                            <a href="#service" class="text-decoration-none text-light">Extensive Wallpaper Collection</a>
                        </p>
                        <p>
                            <a href="#service" class="text-decoration-none text-light">Search and Filter Options</a>
                        </p>
                        <p>
                            <a href="#service" class="text-decoration-none text-light">Wallpaper Submission</a>
                        </p>
                        <p>
                            <a href="#service" class="text-decoration-none text-light">Wallpaper Previews</a>
                        </p>
                        <p>
                            <a href="#service" class="text-decoration-none text-light">Newsletter and Notifications</a>
                        </p>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-md-0 mb-4">
                        <!-- Links -->
                        <h6 class="text-uppercase fw-bold mb-4">Contact</h6>
                        <p class="text-light"><i class="fas fa-location-dot me-3 text-light"></i>69 Munshipara Road, Tongi Bazar</p>
                        <p class="text-light">
                            <i class="fas fa-envelope me-3 text-light"></i>
                            sk.karib35@gmail.com
                        </p>
                        <p class="text-light"><i class="fas fa-phone me-3 text-light"></i> + 01712 988806</p>

                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
            </div>
        </section>
        <!-- Section: Links  -->

        <!-- Copyright -->
        <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
            © 2023 Copyright:
            <a class="text-reset fw-bold text-decoration-none" href="https://karib.netlify.app/">
            <span style="color: orange;">Sayed Farhan Labib Karib</span></a>


        </div>
        <!-- Copyright -->
    </footer>





    <!-- Footer Section end -->