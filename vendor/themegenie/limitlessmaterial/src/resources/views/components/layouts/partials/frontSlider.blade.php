
	<!-- carousel -->


	<div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active" data-bs-interval="10000">
            <img src="{{ asset('frontend/images/slider-1.jpg') }}" class="d-block w-100" alt="...">
				<div class="carousel-caption animate__animated animate__backInUp animate__delay-.5s">
					<h5 class="text-light">Unleash the Beauty</h5>
					<p class="text-light">Discover Your Perfect Wallpaper Wonderland!</p>
					<a class="btn-slide" href="{{route('login')}}"><i class="fa-solid fa-arrow-right m-2"></i>Take Tour</a>
				</div>
			</div>
			<div class="carousel-item" data-bs-interval="2000">
            <img src="{{ asset('frontend/images/slider-2.jpg') }}" class="d-block w-100" alt="...">

				<div class="carousel-caption animate__animated animate__backInUp animate__delay-.5s">
                <h5 class="text-light">Unleash the Beauty</h5>
				<p class="text-light">Discover Your Perfect Wallpaper Wonderland!</p>
				<a class="btn-slide" href="{{route('login')}}"><i class="fa-solid fa-arrow-right m-2"></i>Take Tour</a>
				</div>
			</div>
			<div class="carousel-item">
            <img src="{{ asset('frontend/images/slider-4.jpg') }}" class="d-block w-100" alt="...">

				<div class="carousel-caption animate__animated animate__backInUp animate__delay-.5s">
				<h5 class="text-light">Unleash the Beauty</h5>
				<p class="text-light">Discover Your Perfect Wallpaper Wonderland!</p>
				<a class="btn-slide" href="{{route('login')}}"><i class="fa-solid fa-arrow-right m-2"></i>Take Tour</a>
				</div>
			</div>
		</div>
		<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval"
			data-bs-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Previous</span>
		</button>
		<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval"
			data-bs-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Next</span>
		</button>
	</div>

	<!-- carousel end -->
