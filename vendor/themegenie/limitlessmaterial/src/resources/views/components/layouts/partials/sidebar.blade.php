
<div class="sidebar sidebar-light sidebar-main sidebar-expand-md sidebar-fixed">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				Navigation
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->


			<!-- Sidebar content -->
			<div class="sidebar-content ps ps--active-y">

				<!-- User menu -->
				<div class="sidebar-user-material mt-2">
					<div class="sidebar-user-material-body">
                    {{-- <div style="background-image: url('{{asset('vendor/limitlessmaterial/global_assets/images/backgrounds/bgm.jpg')}}'); background-size: 100% 100%;" class="card-body text-center"> --}}
						<div class="card-body text-center">
						<a href="{{route('profile.show')}}">
							@if(auth()->user()->profile_picture=='https://cdn-icons-png.flaticon.com/512/3135/3135715.png')
								<img src="https://cdn-icons-png.flaticon.com/512/3135/3135715.png" class="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt="">
							@else
							<img src="{{asset('images/profiles')}}/{{auth()->user()->profile_picture}}" class="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt="">
							@endif
							</a>

							<h6 class="mb-0 text-white text-shadow-dark">{{auth()->user()->name}}</h6>
						</div>
                        
													
						<div class="sidebar-user-material-footer">
							<a href="#user-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle legitRipple" data-toggle="collapse"><span>My account</span></a>
						</div>
					</div>
                    {{-- </div> --}}

					<div class="collapse" id="user-nav">
						<ul class="nav nav-sidebar">
							<li class="nav-item">
								<a href="{{route('profile.show')}}" class="nav-link legitRipple">
									<i class="icon-user-plus"></i>
									<span>My profile</span>
								</a>
							</li>
							
							<li class="nav-item">
								<a href="{{route('profile.edit')}}" class="nav-link legitRipple">
									<i class="icon-cog5"></i>
									<span>Account settings</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{route('logout2')}}" class="nav-link legitRipple">
									<i class="icon-switch2"></i>
									<span>Logout</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- /user menu -->


				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">

						<!-- Main -->
						<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs text-center">menu</div> <i class="icon-menu" title="Main"></i></li>
						<li class="nav-item">
							<a href="{{route('display_wallpaper')}}" class="nav-link legitRipple">
								<i class="icon-images2"></i>
								<span>
									Wallpapers
								</span>
							</a>
						</li>
                        
                        <li class="nav-item">
							<a href="{{route('display_my_wallpaper')}}" class="nav-link legitRipple">
								<i class="icon-shutter"></i>
								<span>
									My Wallpaper
								</span>
							</a>
						</li>


                        <li class="nav-item">
							<a href="{{route('upload_wallpaper')}}" class="nav-link legitRipple">
								<i class="icon-arrow-up8"></i>
								<span>
									Upload Wallpaper
								</span>
							</a>
						</li>
						
						
						
						
							<!-- /main -->


					

					</ul>
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->
			
		</div>
