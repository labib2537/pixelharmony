<!-- Global stylesheets -->

<link href="{{ asset('/vendor/limitlessmaterial') }}/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">

<link href="{{ asset('/vendor/limitlessmaterial') }}/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
<link href="{{ asset('/vendor/limitlessmaterial') }}/assets/css/layout.min.css" rel="stylesheet" type="text/css">
<link href="{{ asset('/vendor/limitlessmaterial') }}/assets/css/components.min.css" rel="stylesheet" type="text/css">
<link href="{{ asset('/vendor/limitlessmaterial') }}/assets/css/colors.min.css" rel="stylesheet" type="text/css">

<link href="{{ asset('/vendor/limitlessmaterial') }}/global_assets/css/icons/fontawesome/styles.min.css"
        rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

@stack('css')